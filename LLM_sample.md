---
## Distribution properties
hexsha: "43feaccd3cf8832f10b352b5d2a4206789e48cef"      <!--- Unique ID --->
repo_path: "lib/App/PythonToPerl.pm"                    <!--- Thinking whether we should use repository.web fom dist.ini --->
repo_name: "WBRASWELL/App-PythonToPerl-0.001"           <!--- Name of the repository, helps to identify other files of the same distribution --->
file_version: 0.001                                     <!--- File version obtainable from dist.ini --->
cpan_published_date: "04/09/2023"                       <!--- Date info can be obtained through query --->
size: 741                                              <!--- File character size --->
ext: "pm"                                               <!--- File extension --->
lang: "Perl"                                            <!--- Program language, might not be necessary since this is a pure perl dataset --->
repo_licenses: "GPL_3"                                  <!--- License from dist.ini --->        
max_stars_count: 0                                     <!--- Distribution star --->
max_issues_count: 0                                     <!--- Could be issue number, might not be important --->
max_forks_count: 0                                      <!--- Number of forks, might not be important --->
core_kwalitee: 90.62                                    <!--- Kwalitee score --->

## Distribution file content
content:                                                <!--- code content after filtering out comments --->
’‘’
package App::PythonToPerl;
use strict;
use warnings;
our $VERSION = 0.021_000;
 
use Exporter qw(import);
our @EXPORT    = qw(python_file_to_perl_files__follow_includes python_files_to_perl_files);
 
use Perl::Types;
use OpenAI::API;

sub python_files_to_perl_files {

    { my string::arrayref $RETURN_TYPE };
    (   my OpenAI::API $openai,
        my string::arrayref $python_file_paths,
    ) = @ARG;
 
    my string::arrayref $perl_file_paths = [];

    return $perl_file_paths;
}
 
sub python_file_to_perl_files__follow_includes {
    { my string::arrayref $RETURN_TYPE };
    (   my OpenAI::API $openai,
        my string $python_file_path,
    ) = @ARG;
 
    my string::arrayref $perl_file_paths = [];
    return $perl_file_paths;
}

‘’‘