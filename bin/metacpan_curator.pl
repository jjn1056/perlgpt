#!/usr/bin/env perl
# COPYRIGHT

# Curator for PerlGPT LLM Curator
# Use various quality criteria to filter MetaCPAN distributions, creating a curated list of those accepted for training the PerlGPT LLM

# [[[ HEADER ]]]
#use RPerl;
use strict;
use warnings;
our $VERSION = 0.007_100;

# [[[ INCLUDES ]]]
# system includes
use Perl::Types;
use Data::Dumper;
use Time::HiRes qw(gettimeofday tv_interval);
use POSIX qw( strftime );

# internal includes
use PerlGPT::Curator::MetaCPAN;

# TMP DEBUG, indicate where leaked variables are coming from
#use Devel::LeakTrace;

# TMP DEBUG, track memory usage of every object including where they're created
#use Devel::Leak::Object qw{ GLOBAL_bless };
#$Devel::Leak::Object::TRACKSOURCELINES = 1;

# TMP DEBUG, trace memory leaks with verbose output
#use Test::LeakTrace;
#leaktrace{

# [[[ OPERATIONS ]]]

my number $time_start = gettimeofday;
print 'have $time_start = ', $time_start, ' seconds', "\n";
print 'have start time ', strftime('%Y%m%d %H%M.%S', localtime($time_start)), "\n";

my PerlGPT::Curator::MetaCPAN $metacpan_trainer = PerlGPT::Curator::MetaCPAN->new();

print 'have $metacpan_trainer = ', Dumper($metacpan_trainer), "\n";
#print 'have $metacpan_trainer->{licenses_reject} = ', Dumper($metacpan_trainer->{licenses_reject}), "\n";
#print 'have $metacpan_trainer->{licenses_accept} = ', Dumper($metacpan_trainer->{licenses_accept}), "\n";

my hashref $distributions_accepted = $metacpan_trainer->curate();

print 'have $distributions_accepted = ', Dumper($distributions_accepted), "\n";

my number $time_elapsed = tv_interval([$time_start]);
print 'have $time_elapsed = ', $time_elapsed, ' seconds', "\n";

#};
#} -verbose;

