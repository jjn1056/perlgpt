# COPYRIGHT
# ABSTRACT: PerlGPT LLM Curator Using MetaCPAN Data
# [[[ HEADER ]]]
#use RPerl;
package PerlGPT::Curator::MetaCPAN;
use strict;
use warnings;
our $VERSION = 0.058_000;

# [[[ OO INHERITANCE ]]]
use parent qw(PerlGPT::Curator);
use PerlGPT::Curator;

# [[[ CRITICS ]]]
## no critic qw(ProhibitUselessNoCritic ProhibitMagicNumbers RequireCheckedSyscalls)  # USER DEFAULT 1: allow numeric values & print op
## no critic qw(RequireInterpolationOfMetachars)  # USER DEFAULT 2: allow single-quoted control characters & sigils
## no critic qw(ProhibitConstantPragma ProhibitMagicNumbers)  # USER DEFAULT 3: allow constants

# NEED FIX: rename filehandleref to filehandle in Perl::Types, remove from here
# NEED FIX: rename filehandleref to filehandle in Perl::Types, remove from here
# NEED FIX: rename filehandleref to filehandle in Perl::Types, remove from here

# [[[ SUB-TYPES ]]]
package    # hide from PAUSE indexing
    filehandle;
use strict;
use warnings;
package PerlGPT::Curator::MetaCPAN;

# [[[ INCLUDES ]]]
use Perl::Types;  # use Data::Dumper;  use English;
use Cwd;
use File::Spec::Functions qw(catpath splitpath catfile catdir);
use File::Path qw(remove_tree make_path);
use Parallel::ForkManager;
# phase 0
use LWP::UserAgent;
use JSON::PP;
# phase 1
use Module::CPANTS::Analyse;
use Archive::Tar;
# phase 2
use Perl::Tidy;
# phase 3
use Perl::Critic;
# phase 4
use PPI;
use PPI::Dumper;

# [[[ OO PROPERTIES ]]]
our hashref $properties = {
    # phases of the curation process, for use as path prefixes
    phases => my string::arrayref $TYPED_phases = [qw{
        0_metacpan
        1_kwalitee
        2_tidy
        3_critic
        4_ppi
    }],

    # the base directory, in other words the current working directory when the curator is first executed
    base_directory => my string $TYPED_base_directory = undef,

    # MetaCPAN API URL
    metacpan_api_url => my string $TYPED_metacpan_api_url = 'https://fastapi.metacpan.org/v1/release/_search',

    # TMP DEBUG, DON'T REPEAT KNOWN RESULTS; the approximate count of good results, to be skipped when searching for bad results
    last_good_result => my integer $TYPED_last_good_result = 0,

    # 5,000 results per query is the maximum allowable to avoid the API error "Error fetching data: 416 Request Range Not Satisfiable"
    results_per_query_max => my integer $TYPED_results_per_query_max = 50,  # the maximum number of distributions to return per API query
    queries_max => my integer $TYPED_queries_max = 3,  # the maximum number of API queries to run per curation

    # whether or not to parallelize using Parallel::ForkManager
    parallel => my boolean $TYPED_parallel = 1,

    # the maximum number of Parallel::ForkManager processes
    parallel_processes_max => my integer $TYPED_parallel_processes_max = 4,

# NEED UPGRADE: add authors_accept support
# NEED UPGRADE: add authors_accept support
# NEED UPGRADE: add authors_accept support

    # accept these authors without filters
    authors_accept => my string::arrayref $TYPED_authors_accept = [qw{
        JJNAPIORK
        RMZG
        WBRASWELL
        ZMUGHAL
    }],

    # accept these distributions without filters
    distributions_accept => my string::arrayref $TYPED_distributions_accept = [qw{
        App-PythonToPerl
    }],

    # filter by age
    years_max => my integer $TYPED_years_max = 3,  # the maximum age in years to be considered

    # filter by namespace, reject these namespaces;
    # curate() checks both distribution name (Foo-Bar hyphens) and distribution module (Foo::Bar double colons),
    # include both here for good measure
    namespaces_reject => my string::arrayref $TYPED_namespaces_reject = [qw{
        Acme
    }],

    # filter by CPAN Testers
    testers_min => my integer $TYPED_testers_min = 10,  # the minimum number of CPAN Testers allowed
    testers_fail_percentage_max => my number $TYPED_testers_fail_percentage_max = 0.25,  # the maximum % of failing CPAN Testers allowed, between 0 & 1

    # filter by license, reject these software licenses
    licenses_reject => my string::arrayref $TYPED_licenses_reject = [qw{
        unknown
        open_source
        unrestricted
        gpl_1
        artistic_1
    }],

    # filter by license, accept these software licenses
    licenses_accept => my string::arrayref $TYPED_licenses_accept = [qw{
        perl_5
        http://dev.perl.org/licenses/
        http://opensource.org/licenses/Perl
        gpl_2
        gpl_3
        http://opensource.org/licenses/gpl-license.php
        lgpl_2_1
        lgpl_3_0
        http://www.gnu.org/licenses/agpl.html
        agpl_3
        artistic_2
        http://www.opensource.org/licenses/artistic-license-2.0
        mit
        bsd
        freebsd
        http://opensource.org/licenses/bsd-license.php
        apache_2_0
        mozilla_1_1
    }],

    # sleep this many seconds between downloads
    seconds_between_downloads => my integer $TYPED_seconds_between_downloads = 1,

    # filter by overall CPANTS Kwalitee score;
    # current Kwalitee score range is 0 (low quality) to 33 (high quality)
    kwalitee_min => my integer $TYPED_kwalitee_min = 25,

    # filter by individual CPANTS Kwalitee scores
    kwalitees_require => my string::arrayref $TYPED_kwalitees_require = [qw{
        has_abstract_in_pod
        has_buildtool
        has_readme
        has_tests
        has_tests_in_t_dir
        no_abstract_stub_in_pod
        use_strict
        use_warnings
    }],

    # valid Perl file suffixes, we assume these files contain Perl source code
    file_suffixes => my string::arrayref $TYPED_file_suffixes = [qw{
        .pl
        .PL
        .perl
        .PERL
        .pm
        .PM
        .t
    }],

    # width of lines after Perl::Tidy 
    tidy_line_width => my integer $TYPED_tidy_line_width = 160,

    # filter by Perl::Critic violation severity;
    # current Critic severity range is 1 (low severity, many violations) to 5 (high severity, few violations)
    critic_severity => my integer $TYPED_critic_severity = 4,

    # info about CPAN distributions which are rejected due to errors, warnings, and filters;
    # and distributions which pass all filters and are thus accepted
    distributions_rejected => my hashref $TYPED_distributions_rejected = undef,
    distributions_accepted => my hashref $TYPED_distributions_accepted = undef,
};

# [[[ SUBROUTINES & OO METHODS ]]]

# PGPTTRMC00x
sub curate {
# curate CPAN distributions to reject those which are not suitable to be included as part of the input data set for LLM training;
# return list of accepted CPAN distributions
    { my hashref $RETURN_TYPE };
    ( my PerlGPT::Curator::MetaCPAN $self ) = @ARG;

    # set the base directory
    $self->{base_directory} = getcwd();
    print 'have $self->{base_directory} = \'', $self->{base_directory}, '\'', "\n";

# NEED FIX: add any missing error checking below, correct serialization numbers in these and all other ERROR & WARNING statements, etc
# NEED FIX: add any missing error checking below, correct serialization numbers in these and all other ERROR & WARNING statements, etc
# NEED FIX: add any missing error checking below, correct serialization numbers in these and all other ERROR & WARNING statements, etc

    # error or warning if no MetaCPAN API URL
    if ((not exists  $self->{metacpan_api_url}) or
        (not defined $self->{metacpan_api_url})) {
        die 'ERROR EPGPTTRMC000: non-existent or undefined MetaCPAN API URL, dying';
    }
    elsif ($self->{metacpan_api_url} eq q{}) {
        warn 'WARNING WPGPTTRMC000: empty MetaCPAN API URL';
    }

    # error or warning if no maximum results per MetaCPAN API query
    if ((not exists  $self->{results_per_query_max}) or
        (not defined $self->{results_per_query_max})) {
        die 'ERROR EPGPTTRMC001: non-existent or undefined maximum results per MetaCPAN API query, dying';
    }
    elsif ($self->{results_per_query_max} < 0) {
        warn 'WARNING WPGPTTRMC001: invalid maximum results per MetaCPAN API query';
    }

    # error or warning if no maximum MetaCPAN API queries per curation
    if ((not exists  $self->{queries_max}) or
        (not defined $self->{queries_max})) {
        die 'ERROR EPGPTTRMC002: non-existent or undefined maximum MetaCPAN API queries per curation, dying';
    }
    elsif ($self->{queries_max} < 0) {
        warn 'WARNING WPGPTTRMC002: invalid maximum MetaCPAN API queries per curation';
    }

    # error or warning if no list of distributions to accept without filters
    if ((not exists  $self->{distributions_accept}) or
        (not defined $self->{distributions_accept})) {
        die 'ERROR EPGPTTRMC003: non-existent or undefined list of distributions to accept without filters, dying';
    }
    elsif ((scalar @{$self->{distributions_accept}}) == 0) {
        warn 'WARNING WPGPTTRMC003: empty list of distributions to accept without filters';
    }

    # error or warning if no maximum distribution age in years
    if ((not exists  $self->{years_max}) or
        (not defined $self->{years_max})) {
        die 'ERROR EPGPTTRMC004: non-existent or undefined maximum distribution age in years, dying';
    }
    elsif ($self->{years_max} < 0) {
        warn 'WARNING WPGPTTRMC004: invalid maximum distribution age in years';
    }

    # error or warning if no list of distribution namespaces to reject
    if ((not exists  $self->{namespaces_reject}) or
        (not defined $self->{namespaces_reject})) {
        die 'ERROR EPGPTTRMC005: non-existent or undefined list of distribution namespaces to reject, dying';
    }
    elsif ((scalar @{$self->{namespaces_reject}}) == 0) {
        warn 'WARNING WPGPTTRMC005: empty list of distribution namespaces to reject';
    }

    # error or warning if no minimum number of distribution CPAN Testers
    if ((not exists  $self->{testers_min}) or
        (not defined $self->{testers_min})) {
        die 'ERROR EPGPTTRMC006: non-existent or undefined minimum number of distribution CPAN Testers, dying';
    }
    elsif ($self->{testers_min} < 0) {
        warn 'WARNING WPGPTTRMC006: invalid minimum number of distribution CPAN Testers';
    }

    # error or warning if no maximum percentage of failed distribution CPAN Testers
    if ((not exists  $self->{testers_fail_percentage_max}) or
        (not defined $self->{testers_fail_percentage_max})) {
        die 'ERROR EPGPTTRMC007: non-existent or undefined maximum percentage of failed distribution CPAN Testers, dying';
    }
    elsif ($self->{testers_fail_percentage_max} < 0) {
        warn 'WARNING WPGPTTRMC007: invalid maximum percentage of failed distribution CPAN Testers';
    }

    # error or warning if no list of distribution licenses to reject
    if ((not exists  $self->{licenses_reject}) or
        (not defined $self->{licenses_reject})) {
        die 'ERROR EPGPTTRMC008: non-existent or undefined list of distribution licenses to reject, dying';
    }
    elsif ((scalar @{$self->{licenses_reject}}) == 0) {
        warn 'WARNING WPGPTTRMC008: empty list of distribution licenses to reject';
    }

    # error or warning if no list of distribution licenses to accept
    if ((not exists  $self->{licenses_accept}) or
        (not defined $self->{licenses_accept})) {
        die 'ERROR EPGPTTRMC009: non-existent or undefined list of distribution licenses to accept, dying';
    }
    elsif ((scalar @{$self->{licenses_accept}}) == 0) {
        warn 'WARNING WPGPTTRMC009: empty list of distribution licenses to accept';
    }

    # if parallelized, instantiate fork manager to help mitigate the external memory leaks, caused by (presumably) PPI and/or Perl::Critic
    my Parallel::ForkManager $fork_manager;
    if ($self->{parallel}) { 
        print "[$PID] ", 'PARALLEL: before MetaCPAN API query, about to call Parallel::ForkManager->new()...', "\n";
        $fork_manager = Parallel::ForkManager->new($self->{parallel_processes_max});
    }

    # create empty hashes to store info about CPAN distributions;
    # if parallelized, collects response(s) from child process(es);
    # if this subroutine is successful, will be used to overwrite $self->{distributions_accepted} etc, and returned as return value
    my hashref $distributions_rejected = {};
    my hashref $distributions_accepted = {};

    # if parallelized, set up anonymous subroutine callback for return value data structure retrieval and handling
    if ($self->{parallel}) { $fork_manager->run_on_finish( sub {
        my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $data_structure_reference) = @_;
        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $pid = \'', $pid, '\'', "\n";
        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $exit_code = \'', $exit_code, '\'', "\n";
        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $ident = \'', $ident, '\'', "\n";
        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $exit_signal = \'', $exit_signal, '\'', "\n";
#        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $core_dump = ', Dumper($core_dump), "\n";
        print "[$PID] ", 'PARALLEL: in run_on_finish() callback, received $data_structure_reference = ', Dumper($data_structure_reference), "\n";

        # see what return value the child process sent us, if anything
        if (defined($data_structure_reference)) {  # test rather than assume child sent anything
            my $reftype = ref($data_structure_reference);
            print "[$PID] ", 'PARALLEL: ident \'', $ident, '\' returned a \'', $reftype, '\' reference', "\n";

            # collect retrieved data structures for processing after all children have exited;
            # distributions that encountered errors, warnings, or filters are rejected; all others are accepted
            if ((exists $data_structure_reference->{errored}) or
                (exists $data_structure_reference->{warned}) or
                (exists $data_structure_reference->{filtered})) {
                $distributions_rejected->{$ident} = $data_structure_reference;
                print "[$PID] ", 'PARALLEL, REJECTING: ident \'', $ident, '\' was errored or warned or filtered', "\n";
            }
            else {
                $distributions_accepted->{$ident} = $data_structure_reference;
                print "[$PID] ", 'PARALLEL, ACCEPTING: ident \'', $ident, '\' did not encounter any problems', "\n";
            }
        }
        else {
            print "[$PID] ", 'PARALLEL: ident \'', $ident, '\' did not returned any data', "\n";
        }
    })};

    # create repeatedly-used objects outside loops, to avoid creating our own internal memory leaks
#    my Module::CPANTS::Analyse $kwalitee_analyzer = Module::CPANTS::Analyse->new();
    my Module::CPANTS::Analyse $kwalitee_analyzer;
#    my Archive::Tar $tar_archiver = Archive::Tar->new();
    my Archive::Tar $tar_archiver;
#    my Perl::Critic $perl_criticizer = Perl::Critic->new( -severity => $self->{critic_severity} );
    my Perl::Critic $perl_criticizer;
#    print 'have initial $kwalitee_analyzer = ', Dumper($kwalitee_analyzer), "\n";
#    print 'have initial $tar_archiver = ', Dumper($tar_archiver), "\n";
#    print 'have initial $perl_criticizer = ', Dumper($perl_criticizer), "\n";

    # [[[ PHASE 0: MetaCPAN ]]]
    # [[[ PHASE 0: MetaCPAN ]]]
    # [[[ PHASE 0: MetaCPAN ]]]

    # create User Agent object
    my LWP::UserAgent $user_agent = LWP::UserAgent->new();

# NEED UPGRADE: create new API query for non-filtered distributions in $self->{distributions_accept}, add to $self->{distributions_accepted}
# NEED UPGRADE: create new API query for non-filtered distributions in $self->{distributions_accept}, add to $self->{distributions_accepted}
# NEED UPGRADE: create new API query for non-filtered distributions in $self->{distributions_accept}, add to $self->{distributions_accepted}

    # current year
    my integer $current_year = (localtime)[5] + 1900;

    # calculate the release date for the most recent years
    my integer $N_years_ago = $current_year - $self->{years_max};

    # build the MetaCPAN API query
    my hashref $metacpan_api_query = {
        query => {
            # TMP DEBUG, ONLY SEARCH FOR SPECIFIC DISTRIBUTION(S), CAN NOT COMBINE WITH range QUERY BELOW
#            terms => { distribution => [
#                'Map-Tube-Server',  # NEED TEST: Perl::Critic::Policy::TestingAndDebugging::RequireUseStrict
#                'Object-Pad',  # NEED FIX: "Can't parse code: Encountered unexpected character '196'"
#                'Syntax-Operator-In',  # NEED FIX: "Can't parse code: Encountered unexpected character '226'" /Syntax-Operator-In-0.09/t/02elem-numbery.t
#                'Syntax-Operator-Identical',  # NEED FIX: "Can't parse code: Encountered unexpected character '226'"
#                'XS-Parse-Keyword',  # NEED FIX: "Can't parse code: Encountered unexpected character '226'"
#                'DBM-Deep',  # NEED FIX: "Can't parse code: Encountered unexpected character '225'" /DBM-Deep-2.0019/t/97_dump_file.t
#                'Encode',  # NEED FIX: "Can't parse code: Encountered unexpected character '191'" /Encode-3.20/t/jperl.t
#                'Function-Parameters',  # NEED FIX: "Can't parse code: Encountered unexpected character '195'" /Function-Parameters-2.002004/t/unicode.t
#                'Lingua-JA-Name-Splitter',  # NEED FIX: "Can't parse code: Encountered unexpected character '233'" /Lingua-JA-Name-Splitter-0.12/t/Lingua-JA-Name-Splitter.t
#            ]},

            range => {
                # filter by age
                date => { gte => $N_years_ago . '-01-01', lte => $current_year . '-12-31' }
            },
        },

        from => $self->{last_good_result},  # starting from the last good result, will be first result if set to 0
        size => ($self->{results_per_query_max} - $self->{last_good_result}),  # stay in original results, not accounting for new uploads
        sort   => [{ date => { order => 'desc' }}],  # sort by date in descending order
        filter => { term => { status => 'latest' }},  # only return latest versions of each distribution
    };

# NEED UPGRADE: use scroll API and 'from' field in API query above to loop and call API query $self->{queries_max} times for > 5,000 results
# NEED UPGRADE: use scroll API and 'from' field in API query above to loop and call API query $self->{queries_max} times for > 5,000 results
# NEED UPGRADE: use scroll API and 'from' field in API query above to loop and call API query $self->{queries_max} times for > 5,000 results

    # make the MetaCPAN API request
    my HTTP::Response $metacpan_api_response = $user_agent->post($self->{metacpan_api_url}, Content => encode_json($metacpan_api_query));
#    print 'have $metacpan_api_response = ', Dumper($metacpan_api_response), "\n";

    # check the MetaCPAN API response for errors
    if (not $metacpan_api_response->is_success()) {
        die 'ERROR EPGPTTRMCxxx: problem fetching data; ', $metacpan_api_response->status_line(), ', dying'; 
    }

    # decode the JSON data of the MetaCPAN API response
    my hashref $distributions = decode_json($metacpan_api_response->decoded_content);
#    print 'have $distributions = ', Dumper($distributions), "\n";
    my integer $distributions_count = scalar @{$distributions->{hits}->{hits}};
    print 'have $distributions_count = ', $distributions_count, "\n";

    # how many distributions we have processed
    my integer $distribution_count = 0;

    # brief message containing the decision on why a distribution was rejected
    my string $decision;

# NEED UPGRADE: add ability to redirect in-loop print output to individual files on disk, to ensure parallel processes log separately?
# NEED UPGRADE: add ability to redirect in-loop print output to individual files on disk, to ensure parallel processes log separately?
# NEED UPGRADE: add ability to redirect in-loop print output to individual files on disk, to ensure parallel processes log separately?

    # process the distributions returned by the MetaCPAN API results
    DISTRIBUTION:
    for my hashref $distribution (@{$distributions->{hits}->{hits}}) {
#    print 'have $distribution = ', Dumper($distribution), "\n";
        $distribution_count++;
    print "[$PID] ", 'top of DISTRIBUTION loop, have starting-at-1 $distribution_count = ', $distribution_count, "\n";

        # if parallelized, fork new child process
        if ($self->{parallel}) {
            # fork new child process for each DISTRIBUTION loop iteration, to help mitigate the external memory leaks;
            # each CPAN distribution is processed by a new OS child process,
            # which will contain the memory leaks and will free the leaked memory back to the OS after each distribution is processed
            print "[$PID] ", 'top of DISTRIBUTION loop, about to call $fork_manager->start()...', "\n";
            my integer $child_pid = $fork_manager->start(
                # DEV NOTE: hacky way of passing distribution name or ID as new child process name,
                # before they are actually checked and filtered below, so the child process can be forked at the top of the loop
                # and thus execute the entire DISTRIBUTION loop body instead of starting part-way down into the loop body;
                # this may not be necessary at all, but it seems better to have the fork happen here
                $distribution->{_source}->{distribution} or  # same as $distribution_name saved below
                $distribution->{_id} or  # presumably MetaCPAN's internal ID number?  missing name, will be filtered below
                'MISSING_DISTRIBUTION_NAME_' . $distribution_count  # missing name and ID, give temporary name, will be filtered below
            );

            # parent process skips to next loop iteration,
            # only child processes continue to execute DISTRIBUTION loop body
            if ($child_pid != 0) {
                print "[$PID] ", 'in parent process, have new $child_pid = ', $child_pid, "\n";
                next DISTRIBUTION;
            }
            else {
                print "[$PID] ", 'in new child process, have $PID = ', $PID, "\n";
            }
        }
 
        # mkdir & chdir for phase 0
        $self->phase_mkdir_chdir(0);

        # save distribution ID and type
        $distribution->{_source}->{_id} = $distribution->{_id};
        $distribution->{_source}->{_type} = $distribution->{_type};

        # discard remaining unused data outside $distribution->{_source}
        $distribution = $distribution->{_source};

        # discard unused data inside $distribution
        delete $distribution->{authorized};
        delete $distribution->{deprecated};
        delete $distribution->{first};

        # filter on missing distribution names
        if ((not exists $distribution->{distribution}) or 
            (not defined $distribution->{distribution}) or 
            ($distribution->{distribution} eq q{})) {
            print "[$PID] ", $decision = 'FILTERING: missing distribution name from _id = ' . $distribution->{_id}, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution->{_id}} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on missing module names
        if ((not exists $distribution->{main_module}) or 
            (not defined $distribution->{main_module}) or 
            ($distribution->{main_module} eq q{})) {
            print "[$PID] ", $decision = 'FILTERING: missing main module from distribution = \'' . $distribution->{distribution}, '\'', "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution->{distribution}} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on missing distribution download URLs
        if ((not exists $distribution->{download_url}) or 
            (not defined $distribution->{download_url}) or 
            ($distribution->{download_url} eq q{})) {
            print "[$PID] ", $decision = 'FILTERING: missing download_url from distribution = ' . $distribution->{distribution}, '\'', "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution->{distribution}} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # create shortcuts to distribution name, module, and download URL for frequent use
        my string $distribution_name = $distribution->{distribution};
        my string $distribution_module = $distribution->{main_module};
        my string $download_url = $distribution->{download_url};
        print "[$PID] ", 'have $distribution_name = \'', $distribution_name, '\'', "\n";
        print "[$PID] ", 'have $distribution_module = \'', $distribution_module, '\'', "\n";
        print "[$PID] ", 'have $download_url = \'', $download_url, '\'', "\n";

        # assemble MetaCPAN metadata path
        my string $distribution_metacpan_path = catfile(getcwd(), ($distribution_name . '-METADATA.pm'));
        print "[$PID] ", 'have $distribution_metacpan_path = \'', $distribution_metacpan_path, '\'', "\n";

        # do not repeat distribution MetaCPAN metadata writing if already written
        if ((-e $distribution_metacpan_path) and
            (-f $distribution_metacpan_path)) {
            print "[$PID] ", 'NOT SAVING: MetaCPAN metadata file \'', $distribution_metacpan_path, '\' already exists', "\n";
        }
        else {
            # save distribution MetaCPAN metadata in phase 0 directory
            print "[$PID] ", 'SAVING: writing MetaCPAN metadata file \'', $distribution_metacpan_path, '\'', "\n";
            open(my filehandle $DISTRIBUTION_METACPAN_FILE, '>', $distribution_metacpan_path) or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening distribution MetaCPAN metadata file \'' . $distribution_metacpan_path . '\' for writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };
            print $DISTRIBUTION_METACPAN_FILE '$distribution = ', Dumper($distribution), '1;', "\n";
            close $DISTRIBUTION_METACPAN_FILE or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing distribution MetaCPAN metadata file \'' . $distribution_metacpan_path . '\' after writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };
        }

        # filter on already-accepted distributions;
        # check this distribution if it is not already accepted,
        # because there may be multiple different versions and the newest version may not pass the filters to be accepted;
        # DEV NOTE: this filter should not normally be triggered with API query filtering out all but the latest versions only
        if (exists $distributions_accepted->{$distribution_name}) {
            print "[$PID] ", $decision = 'FILTERING: already accepted, ' . $distribution_name . ' from ' . $download_url, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on rejected namespace
        foreach my string $namespace_reject (@{$self->{namespaces_reject}}) {
#            print "[$PID] ", 'have $namespace_reject = \'', $namespace_reject, '\'', "\n";
            if (((substr $distribution_name,   0, (length $namespace_reject)) eq $namespace_reject) or
                ((substr $distribution_module, 0, (length $namespace_reject)) eq $namespace_reject)) {
                print "[$PID] ", $decision = 'FILTERING: rejected distribution namespace \'' . $namespace_reject . '\', ' . $distribution_name, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
                next DISTRIBUTION;
            }
        }

        # filter on missing distribution CPAN Testers;
        # DEV NOTE: the MetaCPAN API calls them "tests" instead of "testers",
        # this is confusing because it is a count of the CPAN Testers, not a count of the number of test cases;
        # not to be confused with Kwalitee has_tests and has_tests_in_t_dir, which are looking for test cases
        if ((not exists $distribution->{tests}->{pass}) or 
            (not defined $distribution->{tests}->{pass}) or 
            ($distribution->{tests}->{pass} < 0) or
            (not exists $distribution->{tests}->{fail}) or 
            (not defined $distribution->{tests}->{fail}) or 
            ($distribution->{tests}->{fail} < 0)) {
            print "[$PID] ", $decision = 'FILTERING: missing or incomplete CPAN Testers data, ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # calculate total number of CPAN Testers
        my integer $testers_pass = $distribution->{tests}->{pass};
        my integer $testers_fail = $distribution->{tests}->{fail};
        my integer $testers_total = $testers_pass + $testers_fail;

# NEED FIX: for brand new disributions without enough testers yet, fall back to previous version
# NEED FIX: for brand new disributions without enough testers yet, fall back to previous version
# NEED FIX: for brand new disributions without enough testers yet, fall back to previous version

        # filter on too few total CPAN Testers
        if ($testers_total < $self->{testers_min}) {
            print "[$PID] ", $decision = 'FILTERING: too few CPAN Testers, ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # calculte percentage of failed CPAN Testers
        my number $testers_fail_percentage = $testers_fail / $testers_total;
        print "[$PID] ", 'have $testers_fail_percentage = ', $testers_fail_percentage, "\n";

        # filter on too many failing CPAN Testers
        if ($testers_fail_percentage > $self->{testers_fail_percentage_max}) {
            print "[$PID] ", $decision = 'FILTERING: too many failing CPAN Testers, ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on missing distribution license
        if ((not exists $distribution->{license}) or 
            (not defined $distribution->{license}) or 
            ((scalar @{$distribution->{license}} == 0)) or
            ($distribution->{license}->[0] eq q{})) {
            print "[$PID] ", $decision = 'FILTERING: missing distribution license(s), ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on rejected distribution licenses
        my integer $distribution_licenses_count = scalar @{$distribution->{license}};
        my integer $distribution_licenses_rejected_count = 0;
        foreach my string $license_reject (@{$self->{licenses_reject}}) {
            foreach my string $distribution_license (@{$distribution->{license}}) {
                if ($distribution_license eq $license_reject) {
                    print "[$PID] ", 'have rejected distribution license \'', $distribution_license, '\', ', $distribution_name, "\n";
                    $distribution_licenses_rejected_count++;
                }
            }
        }
        if ($distribution_licenses_count == $distribution_licenses_rejected_count) {
            print "[$PID] ", $decision = 'FILTERING: rejected all distribution licenses (\'' . join('\', \'', @{$distribution->{license}}) . '\'), ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }
        elsif ($distribution_licenses_count < $distribution_licenses_rejected_count) {
            print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: more distribution licenses rejected than should exist, dying';
            if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
            next DISTRIBUTION;
        }

        # filter on unknown distribution licenses
        my integer $distribution_licenses_accepted_count = 0;
        foreach my string $license_accept (@{$self->{licenses_accept}}) {
            foreach my string $distribution_license (@{$distribution->{license}}) {
                if ($distribution_license eq $license_accept) {
                    print "[$PID] ", 'have accepted distribution license \'', $distribution_license, '\', ', $distribution_name, "\n";
                    $distribution_licenses_accepted_count++;
                }
            }
        }
        if ($distribution_licenses_accepted_count == 0) {
            print "[$PID] ", $decision = 'FILTERING: no accepted distribution licenses (\'' . join('\', \'', @{$distribution->{license}}) . '\'), ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }
        elsif ($distribution_licenses_count < $distribution_licenses_accepted_count) {
            print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: more distribution licenses accepted than should exist, dying';
            if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
            next DISTRIBUTION;
        }

        # assemble tarball name & path
        my string $distribution_tarball_name = $distribution_name . '.tar.gz';
        my string $distribution_tarball_path = catfile(getcwd(), $distribution_tarball_name);
        print "[$PID] ", 'have $distribution_tarball_name = \'', $distribution_tarball_name, '\'', "\n";
        print "[$PID] ", 'have $distribution_tarball_path = \'', $distribution_tarball_path, '\'', "\n";

        # do not repeat distribution tarball download if already downloaded
        if ((-e $distribution_tarball_path) and
            (-f $distribution_tarball_path)) {
            print "[$PID] ", 'NOT DOWNLOADING: Tarball \'', $distribution_tarball_path, '\' already exists', "\n";
        }
        else {
            # sleep between downloads to avoid $download_response->status_line() of...
            # 500 Can't connect to cpan.metacpan.org:443
            print "[$PID] ", 'SLEEPING BEFORE DOWNLOAD...', "\n";
            sleep $self->{seconds_between_downloads};

            # download the distribution tarball
            print "[$PID] ", 'DOWNLOADING: Phase 0 filters passed, downloading ', $distribution_name, ' from ', $download_url, "\n";
            my hashref $download_response = $user_agent->get($download_url, ':content_file' => $distribution_tarball_name);

# NEED FIX: retry failed downloads; detect & retry partial downloads
# NEED FIX: retry failed downloads; detect & retry partial downloads
# NEED FIX: retry failed downloads; detect & retry partial downloads
 
            # filter on download errors
            if (not $download_response->is_success()) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: problem downloading ' . $distribution_tarball_name . ', ' . $distribution_name . '; ' . $download_response->status_line(), "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }
        }

        # [[[ PHASE 1: CPANTS Kwalitee ]]]
        # [[[ PHASE 1: CPANTS Kwalitee ]]]
        # [[[ PHASE 1: CPANTS Kwalitee ]]]

        # mkdir & chdir for phase 1
        $self->phase_mkdir_chdir(1);

        # declare variable to hold output of Kwalitee analysis; defined below by either run() and d(), or pre-saved output of d()
        my hashref $distribution_kwalitee;

        # assemble Kwalitee analysis path
        my string $distribution_kwalitee_path = catfile(getcwd(), ($distribution_name . '-KWALITEE.pm'));
        print "[$PID] ", 'have $distribution_kwalitee_path = \'', $distribution_kwalitee_path, '\'', "\n";

        # do not repeat distribution Kwalitee analysis if already analyzed
        if ((-e $distribution_kwalitee_path) and
            (-f $distribution_kwalitee_path)) {
            print "[$PID] ", 'NOT ANALYZING: Kwalitee analysis file \'', $distribution_kwalitee_path, '\' already exists', "\n";

            # read in already-saved distribution Kwalitee analysis in phase 1 directory
            open(my filehandle $DISTRIBUTION_KWALITEE_FILE, '<', $distribution_kwalitee_path) or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening already-saved distribution Kwalitee analysis file \'' . $distribution_kwalitee_path . '\' for reading; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };
            my string $distribution_kwalitee_lines;
            while (my string $distribution_kwalitee_line = readline($DISTRIBUTION_KWALITEE_FILE)) {
                $distribution_kwalitee_lines .= $distribution_kwalitee_line;
            }
            print "[$PID] ", 'have $distribution_kwalitee_lines = ', "\n", $distribution_kwalitee_lines, "\n";
            close $DISTRIBUTION_KWALITEE_FILE or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing already-saved distribution Kwalitee analysis file \'' . $distribution_kwalitee_path . '\' after reading; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };

            # eval to access value of $distribution_kwalitee from already-saved file;
            # prepend declaration of default Dumper variable name $VAR1 to avoid error...
            # Global symbol "$VAR1" requires explicit package name (did you forget to declare "my $VAR1"?)
            eval 'my $VAR1; ' . $distribution_kwalitee_lines;

            # filter on eval errors
            if ($EVAL_ERROR) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: problem accessing already-saved distribution Kwalitee analysis read in from file \'' . $distribution_kwalitee_path . '\'; ' . $EVAL_ERROR, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }
            print "[$PID] ", 'have pre-saved $distribution_kwalitee = ', Dumper($distribution_kwalitee), "\n";
        }
        else {
            # free memory from repeatedly-used object just before re-creating new object, to avoid creating our own internal memory leaks
            undef $kwalitee_analyzer;
            $kwalitee_analyzer = Module::CPANTS::Analyse->new({ dist => $distribution_tarball_path });

            # analyze the CPANTS Kwalitee of this distribution
            print "[$PID] ", 'ANALYZING: about to call $kwalitee_analyzer->run()...', "\n";
            $kwalitee_analyzer->run();
#            print "[$PID] ", 'have $kwalitee_analyzer = ', Dumper($kwalitee_analyzer), "\n";
            $distribution_kwalitee = $kwalitee_analyzer->d();
            print "[$PID] ", 'have $distribution_kwalitee = ', Dumper($distribution_kwalitee), "\n";

            # save distribution Kwalitee analysis in phase 1 directory
            print "[$PID] ", 'SAVING: writing Kwalitee analysis file \'', $distribution_kwalitee_path, '\'', "\n";
            open(my filehandle $DISTRIBUTION_KWALITEE_FILE, '>', $distribution_kwalitee_path) or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening distribution Kwalitee analysis file \'' . $distribution_kwalitee_path . '\' for writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };
            print $DISTRIBUTION_KWALITEE_FILE '$distribution_kwalitee = ', Dumper($distribution_kwalitee), '1;', "\n";
            close $DISTRIBUTION_KWALITEE_FILE or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing distribution Kwalitee analysis file \'' . $distribution_kwalitee_path . '\' after writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };
        }

        # filter on CPANTS Kwalitee errors
        if ((not exists $distribution_kwalitee->{kwalitee}->{kwalitee}) or 
           (not defined $distribution_kwalitee->{kwalitee}->{kwalitee})) {
            print "[$PID] ", $decision = 'WARNING & FILTERING: problem with overall CPANTS Kwalitee score, ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
            next DISTRIBUTION;
        }

        # filter on overall CPANTS Kwalitee score
        if ($distribution_kwalitee->{kwalitee}->{kwalitee} < $self->{kwalitee_min}) {
            print "[$PID] ", $decision = 'FILTERING: too low CPANTS Kwalitee score ' . $distribution_kwalitee->{kwalitee}->{kwalitee} . ', minimum is ' . $self->{kwalitee_min} . ', ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
            next DISTRIBUTION;
        }

        # filter on individual CPANTS Kwalitee scores
        foreach my string $kwalitee_require (@{$self->{kwalitees_require}}) {
            # filter on individual CPANTS Kwalitee error
            if ((not exists $distribution_kwalitee->{kwalitee}->{$kwalitee_require}) or
                (not defined $distribution_kwalitee->{kwalitee}->{$kwalitee_require})) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: problem with individual CPANTS Kwalitee score \'' . $kwalitee_require . '\', ' . $distribution_name, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }

            # filter on individual CPANTS Kwalitee score
            if (not $distribution_kwalitee->{kwalitee}->{$kwalitee_require}) {
                print "[$PID] ", $decision = 'FILTERING: bad individual CPANTS Kwalitee score \'' . $kwalitee_require . '\', ' . $distribution_name, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { filtered => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { filtered => $decision };  # filtered distributions are rejected
                next DISTRIBUTION;
            }
        }

        # free memory from repeatedly-used object just before re-creating new object, to avoid creating our own internal memory leaks
        undef $tar_archiver;
        $tar_archiver = Archive::Tar->new();

        # read the distribution's tarball archive;
        # DEV NOTE: the phase 1 tarball has already been unzipped into $kwalitee_analyzer->{distdir},
        # but Module::CPANTS::Analyse does not preserve the Archive::Tar object with all files already read into memory,
        # so untar it again and load the source files into memory for subsequent use by Perl::Critic;
        print "[$PID] ", 'about to call $tar_archiver->read()...', "\n";
        my hashref::arrayref $distribution_tarball_files_read = [$tar_archiver->read($distribution_tarball_path)];
        my integer $distribution_tarball_files_count = scalar @{$distribution_tarball_files_read};
        my string $distribution_tarball_files_path = catfile(getcwd(), $distribution_tarball_files_read->[0]->{name});
#        print "[$PID] ", 'have $distribution_tarball_files_read = ', Dumper($distribution_tarball_files_read), "\n";
        print "[$PID] ", 'have $distribution_tarball_files_count = ', $distribution_tarball_files_count, "\n";
        print "[$PID] ", 'have $distribution_tarball_files_path = ', $distribution_tarball_files_path, "\n";

# NEED UPGRADE: re-download or otherwise handle tarball errors?  "invalid tarball header" or similar???
# NEED UPGRADE: re-download or otherwise handle tarball errors?  "invalid tarball header" or similar???
# NEED UPGRADE: re-download or otherwise handle tarball errors?  "invalid tarball header" or similar???

        # filter on archive read errors
        if ($distribution_tarball_files_count <= 0) {
            print "[$PID] ", $decision = 'WARNING & FILTERING: problem reading tarball archive file \'' . $distribution_tarball_path . '\', ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
            next DISTRIBUTION;
        }

        # force repeat of distribution tarball extraction if already extracted, in order to build Archive::Tar::File objects in memory
        if ((-e $distribution_tarball_files_path) and
            (-d $distribution_tarball_files_path)) {
            print "[$PID] ", 'DELETING: directory \'', $distribution_tarball_files_path, '\' already exists, deleting and recreating', "\n";

            # use eval to capture remove_tree() errors instead of dying;
            # NEED ANSWER: is eval sufficient, or do we need to use the 'error' option as specificed in the File::Path docs?
            my integer $distribution_tarball_files_deleted_count;
            eval {
                $distribution_tarball_files_deleted_count = remove_tree($distribution_tarball_files_path);
            };

            # filter on remove_tree() errors
            if ($EVAL_ERROR) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: problem removing directory \'' . $distribution_tarball_files_path . '\'; ' . $EVAL_ERROR, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }
            elsif ($distribution_tarball_files_deleted_count <= 0) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: no files removed from directory \'' . $distribution_tarball_files_path . '\'', "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }
        }

        # unzip and extract the distribution's tarball archive;
        # DEV NOTE: $distribution_tarball_files contains both the compressed & the raw uncompressed data for all files in the tarball!!!
        # do NOT store multiple $distribution_tarball_files in memory, to avoid using all memory and freezing OS
        print "[$PID] ", 'about to call $tar_archiver->extract()...', "\n";
        my hashref::arrayref $distribution_tarball_files = [ $tar_archiver->extract() ];
#        print "[$PID] ", 'have $distribution_tarball_files = ', Dumper($distribution_tarball_files), "\n";

        # filter on archive extract errors
        if (not scalar @{$distribution_tarball_files}) {
            print "[$PID] ", $decision = 'WARNING & FILTERING: problem extracting tarball archive file \'' . $distribution_tarball_path . '\', ' . $distribution_name, "\n";
            if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
            $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
            next DISTRIBUTION;
        }

# THEN HERE: run test suite, filter on failures?
# THEN HERE: run test suite, filter on failures?
# THEN HERE: run test suite, filter on failures?

        # [[[ PHASE 2: Perl::Tidy ]]]
        # [[[ PHASE 2: Perl::Tidy ]]]
        # [[[ PHASE 2: Perl::Tidy ]]]

        # mkdir & chdir for phase 2
        $self->phase_mkdir_chdir(2);

        # tidy via Perl::Tidy formatting best practices, all the Perl files in this distribution
        DISTRIBUTION_FILE_TO_TIDY:
        foreach my Archive::Tar::File $distribution_file (@{$distribution_tarball_files}) {
            print "[$PID] ", 'top of phase 2 DISTRIBUTION_FILE_TO_TIDY loop', "\n";
#            print "[$PID] ", 'have $distribution_file = ', Dumper($distribution_file), "\n";

            my string $distribution_file_name = $distribution_file->{name};
            my string $distribution_file_path = catfile(getcwd(), $distribution_file->{prefix}, $distribution_file->{name});
            print "[$PID] ", 'have $distribution_file_name = \'', $distribution_file_name, '\'', "\n";
            print "[$PID] ", 'have $distribution_file_path = \'', $distribution_file_path, '\'', "\n";

            # save path of unmodified file, for potential use in phase 4
            $distribution_file->{path} = $distribution_file_path; 

            # can not tidy the tarball file itself; should be the first Archive::Tar::File object, should not have a 'data' property
            if ((not exists $distribution_file->{data}) or
                (not defined $distribution_file->{data})) {
                print "[$PID] ", 'no distribution file data, presumably a directory, no Perl::Tidy for this directory', "\n";
                next DISTRIBUTION_FILE_TO_TIDY;
            }
            elsif ($distribution_file->{data} eq q{}) {
                print "[$PID] ", 'empty distribution file data, presumably an empty file, no Perl::Tidy for this empty file', "\n";
                next DISTRIBUTION_FILE_TO_TIDY;
            }

# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix
# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix
# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix

            # do not tidy non-Perl suffix files 
            $distribution_file->{is_perl} = 0;
            DISTRIBUTION_FILE_TO_TIDY_FILE_SUFFIX:
            foreach my string $file_suffix (@{$self->{file_suffixes}}) {
                if ((substr $distribution_file_name, (-1 * (length $file_suffix))) eq $file_suffix) {
                    $distribution_file->{is_perl} = 1;
                    print "[$PID] ", 'Perl file suffix, presumably Perl source code, yes Perl::Tidy for this file', "\n";
                    last DISTRIBUTION_FILE_TO_TIDY_FILE_SUFFIX;
                }
            }
            if (not $distribution_file->{is_perl}) {
                print "[$PID] ", 'non-Perl file suffix, presumably not Perl source code, no Perl::Tidy for this file', "\n";
                next DISTRIBUTION_FILE_TO_TIDY;
            }

            print "[$PID] ", 'TIDYING: about to call Perl::Tidy::perltidy()...', "\n";

            # use eval to capture Perl::Tidy errors instead of dying
            my string $distribution_file_tidied_data = undef;
            eval {
                # tidy this individual file;
                # DEV NOTE: must pass scalar reference as input to perltidy() when providing Perl source code,
                # otherwise it believes you are providing a Perl file path
                my string $tidy_stderr_string = undef;
                my integer $tidy_errored   = Perl::Tidy::perltidy(
                    argv        => q{-pbp --ignore-side-comment-lengths --converge -l=} . $self->{tidy_line_width} . 
                                        q{ -b -nst -bext='/' -q -se},
                    source      => \$distribution_file->{data},
                    destination => \$distribution_file_tidied_data,
                    stderr      => \$tidy_stderr_string,
                );
                # NEED ANSWER: is the following if() the correct logic for handling Perl::Tidy errors?
                if ($tidy_errored or $tidy_stderr_string) {  # both minor and major non-fatal errors
                    print "[$PID] ", $decision = 'WARNING & FILTERING: problem tidying file \'' . $distribution_file_path . '\'; ' . $tidy_stderr_string, "\n";
                    if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                    next DISTRIBUTION;
                }
            };

            # filter on Perl::Tidy errors
            if ($EVAL_ERROR) {
                print "[$PID] ", $decision = 'WARNING & FILTERING: problem tidying file \'' . $distribution_file_path . '\'; ' . $EVAL_ERROR, "\n";
                if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                next DISTRIBUTION;
            }
            print "[$PID] ", 'have $distribution_file_tidied_data = ', "\n", $distribution_file_tidied_data, "\n";

            # assemble tidied Perl path; include line width number in file name, to allow for multiple curations at different line widths
            my string $distribution_file_tidied_name = $distribution_file->{name} . '-TIDY' . $self->{tidy_line_width} . '.pm';
            my string $distribution_file_tidied_path = catfile(getcwd(), $distribution_file->{prefix}, $distribution_file_tidied_name);
            print "[$PID] ", 'have $distribution_file_tidied_path = \'', $distribution_file_tidied_path, '\'', "\n";

            # assemble tidied Perl parent directory path;
            # split the current file name off its own path to leave behind only the parent directory
            my string $distribution_file_tidied_directory_path =
                catpath(
                    (splitpath(
                        $distribution_file_path
                    ))[0, 1],  # splitpath returns ( $volume, $directory, $file ), but we want to discard $file, so slice it off
                    q{}  # replace $file with empty string
                );
            print "[$PID] ", 'have $distribution_file_tidied_directory_path = \'', $distribution_file_tidied_directory_path, '\'', "\n";

            # create tidied Perl parent directory with error checking
            $self->mkdir_error_check($distribution_file_tidied_directory_path);

            # save tidied Perl files in phase 2 directory, open
            print "[$PID] ", 'SAVING: writing tidied Perl file \'', $distribution_file_tidied_path, '\'', "\n";
            open(my filehandle $DISTRIBUTION_FILE_TIDY_FILE, '>', $distribution_file_tidied_path) or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening distribution file tidied Perl file \'' . $distribution_file_tidied_path . '\' for writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };

            # save name & path & data of tidied file in memory, thereby marking this file as having been tidied
            $distribution_file->{tidied_name} = $distribution_file_tidied_name; 
            $distribution_file->{tidied_path} = $distribution_file_tidied_path; 
            $distribution_file->{tidied_data} = $distribution_file_tidied_data;

            # save tidied Perl files in phase 2 directory, write
            print $DISTRIBUTION_FILE_TIDY_FILE $distribution_file->{tidied_data};

            # save tidied Perl files in phase 2 directory, close
            close $DISTRIBUTION_FILE_TIDY_FILE or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing distribution file tidied Perl file \'' . $distribution_file_tidied_path . '\' after writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };

# THEN HERE: run test suite again to ensure no errors are introduced by tidying, filter on failures?
# THEN HERE: run test suite again to ensure no errors are introduced by tidying, filter on failures?
# THEN HERE: run test suite again to ensure no errors are introduced by tidying, filter on failures?

        }  # end of phase 2 DISTRIBUTION_FILE_TO_TIDY loop

        # [[[ PHASE 3: Perl::Critic ]]]
        # [[[ PHASE 3: Perl::Critic ]]]
        # [[[ PHASE 3: Perl::Critic ]]]

        # mkdir & chdir for phase 3
        $self->phase_mkdir_chdir(3);

        # paths of all distribution Critic violation files 
        my hashref $distribution_critic_paths = [];

        # criticize the Perl::Critic best practices of all the Perl files in this distribution
        DISTRIBUTION_FILE_TO_CRITICIZE:
        foreach my Archive::Tar::File $distribution_file (@{$distribution_tarball_files}) {
            print "[$PID] ", 'top of phase 3 DISTRIBUTION_FILE_TO_CRITICIZE loop', "\n";
#            print "[$PID] ", 'have $distribution_file = ', Dumper($distribution_file), "\n";

            my string $distribution_file_name = $distribution_file->{name};
            my string $distribution_file_path = catfile(getcwd(), $distribution_file->{prefix}, $distribution_file->{name});
            print "[$PID] ", 'have $distribution_file_name = \'', $distribution_file_name, '\'', "\n";
            print "[$PID] ", 'have $distribution_file_path = \'', $distribution_file_path, '\'', "\n";

            # save path of unmodified file, for potential use in phase 4
            $distribution_file->{path} = $distribution_file_path; 

            # do not criticize the tarball file itself; should be the first Archive::Tar::File object, should not have a 'data' property
            if ((not exists $distribution_file->{data}) or
                (not defined $distribution_file->{data})) {
                print "[$PID] ", 'no distribution file data, presumably a directory, no Perl::Critic for this directory', "\n";
                next DISTRIBUTION_FILE_TO_CRITICIZE;
            }
            elsif ($distribution_file->{data} eq q{}) {
                print "[$PID] ", 'empty distribution file data, presumably an empty file, no Perl::Critic for this empty file', "\n";
                next DISTRIBUTION_FILE_TO_CRITICIZE;
            }

# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix
# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix
# NEED UPGRADE: detect Perl source code inside non-Perl suffix files and files w/out any suffix

# NEED UPGRADE: check for .pod & .POD files, run podchecker
# NEED UPGRADE: check for .pod & .POD files, run podchecker
# NEED UPGRADE: check for .pod & .POD files, run podchecker

            # do not criticize non-Perl suffix files 
            $distribution_file->{is_perl} = 0;
            DISTRIBUTION_FILE_TO_CRITICIZE_FILE_SUFFIX:
            foreach my string $file_suffix (@{$self->{file_suffixes}}) {
                if ((substr $distribution_file_name, (-1 * (length $file_suffix))) eq $file_suffix) {
                    $distribution_file->{is_perl} = 1;
                    print "[$PID] ", 'Perl file suffix, presumably Perl source code, yes Perl::Critic for this file', "\n";
                    last DISTRIBUTION_FILE_TO_CRITICIZE_FILE_SUFFIX;
                }
            }   
            if (not $distribution_file->{is_perl}) {
                print "[$PID] ", 'non-Perl file suffix, presumably not Perl source code, no Perl::Critic for this file', "\n";
                next DISTRIBUTION_FILE_TO_CRITICIZE;
            }

            # declare variable to hold input Perl source code;
            # if modified (tidied) version of this file exists, select it
            my string $distribution_file_data;
            if ((exists $distribution_file->{tidied_data}) and
                (defined $distribution_file->{tidied_data})) {
                print "[$PID] ", 'have modified (tidied) Perl source code', "\n";
                $distribution_file_data = $distribution_file->{tidied_data};
            }
            # otherwise, select original unmodified (untidied) version
            else {
                print "[$PID] ", 'have unmodified (untidied) Perl source code', "\n";
                $distribution_file_data = $distribution_file->{data};
            }

            # declare variable to hold output of Critic violations; defined below by either critique(), or pre-saved output of critique()
            my arrayref $distribution_file_critic_violations;

            # assemble Critic violations path; include severity number in file name, to allow for multiple curations at different severities
            my string $distribution_file_critic_path = catfile(getcwd(), $distribution_file->{prefix}, ($distribution_file->{name} . '-CRITIC' . $self->{critic_severity} . '.pm'));
            print "[$PID] ", 'have $distribution_file_critic_path = \'', $distribution_file_critic_path, '\'', "\n";

            # save all Critic violation paths
            push @{$distribution_critic_paths}, $distribution_file_critic_path;

            # do not repeat distribution Critic violations criticism if already criticized
            if ((-e $distribution_file_critic_path) and
                (-f $distribution_file_critic_path)) {
                print "[$PID] ", 'NOT CRITICIZING: Critic violations file \'', $distribution_file_critic_path, '\' already exists', "\n";

                # read in already-saved distribution file Critic violations in phase 3 directory
                open(my filehandle $DISTRIBUTION_FILE_CRITIC_FILE, '<', $distribution_file_critic_path) or do {
                    print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening already-saved distribution file Critic violations file \'' . $distribution_file_critic_path . '\' for reading; ' . $OS_ERROR . '; dying';
                    if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                    next DISTRIBUTION;
                };
                my string $distribution_file_critic_lines;
                while (my string $distribution_file_critic_line = readline($DISTRIBUTION_FILE_CRITIC_FILE)) {
                    $distribution_file_critic_lines .= $distribution_file_critic_line;
                }
                print "[$PID] ", 'have $distribution_file_critic_lines = ', $distribution_file_critic_lines, "\n";
                close $DISTRIBUTION_FILE_CRITIC_FILE or do {
                    print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing already-saved distribution file Critic violations file \'' . $distribution_file_critic_path . '\' after reading; ' . $OS_ERROR . '; dying';
                    if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                    next DISTRIBUTION;
                };

                # eval to access value of $distribution_file_critic_violations from already-saved file;
                # prepend declaration of default Dumper variable name $VAR1 to avoid error...
                # Global symbol "$VAR1" requires explicit package name (did you forget to declare "my $VAR1"?)
                eval 'my $VAR1; ' . $distribution_file_critic_lines;

                # filter on eval errors
                if ($EVAL_ERROR) {
                    print "[$PID] ", $decision = 'WARNING & FILTERING: problem accessing already-saved distribution file Critic violations read in from file \'' . $distribution_file_critic_path . '\'; ' . $EVAL_ERROR, "\n";
                    if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                    next DISTRIBUTION;
                }
                print "[$PID] ", 'have pre-saved $distribution_file_critic_violations = ', Dumper($distribution_file_critic_violations), "\n";
            }
            else {
                # free memory from repeatedly-used object just before re-creating new object, to avoid creating our own internal memory leaks
                undef $perl_criticizer;
                $perl_criticizer = Perl::Critic->new( -severity => $self->{critic_severity} ); 

                print "[$PID] ", 'CRITICIZING: about to call $perl_criticizer->critique()...', "\n";

# NEED FIX: crash in call to critique() below, caused by UTF8 characters in Syntax-Operator-In-0.09/t/02elem-numbery.t, "Can't parse code: Encountered unexpected character '226'"; and other similar crashes caused by other distributions & character numbers
# NEED FIX: crash in call to critique() below, caused by UTF8 characters in Syntax-Operator-In-0.09/t/02elem-numbery.t, "Can't parse code: Encountered unexpected character '226'"; and other similar crashes caused by other distributions & character numbers
# NEED FIX: crash in call to critique() below, caused by UTF8 characters in Syntax-Operator-In-0.09/t/02elem-numbery.t, "Can't parse code: Encountered unexpected character '226'"; and other similar crashes caused by other distributions & character numbers

                # use eval to capture Perl::Critic errors instead of dying
                eval {
                    # criticize this individual file;
                    # DEV NOTE: must pass scalar reference as input to critique() when providing Perl source code,
                    # otherwise it believes you are providing a Perl file path
                    $distribution_file_critic_violations = [ $perl_criticizer->critique(\$distribution_file_data) ];
                };

                # filter on Perl::Critic errors
                if ($EVAL_ERROR) {
                    print "[$PID] ", $decision = 'WARNING & FILTERING: problem criticizing file \'' . $distribution_file_path . '\'; ' . $EVAL_ERROR, "\n";
                    if ($self->{parallel}) { $fork_manager->finish(0, { warned => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { warned => $decision };  # warned distributions are rejected
                    next DISTRIBUTION;
                }
                print "[$PID] ", 'have $distribution_file_critic_violations = ', Dumper($distribution_file_critic_violations), "\n";

                # assemble Critic violations parent directory path;
                # split the current file name off its own path to leave behind only the parent directory
                my string $distribution_file_critic_directory_path = 
                    catpath( 
                        (splitpath( 
                            $distribution_file_path
                        ))[0, 1],  # splitpath returns ( $volume, $directory, $file ), but we want to discard $file, so slice it off
                        q{}  # replace $file with empty string
                    );
                print "[$PID] ", 'have $distribution_file_critic_directory_path = \'', $distribution_file_critic_directory_path, '\'', "\n";

                # create Critic violations parent directory with error checking
                $self->mkdir_error_check($distribution_file_critic_directory_path);

# NEED FIX: properly handle existing file suffixes so 'Makefile.PL' becomes 'Makefile-PL-CRITIC.pm' (or similar) not 'Makefile.PL-CRITIC.pm'
# NEED FIX: properly handle existing file suffixes so 'Makefile.PL' becomes 'Makefile-PL-CRITIC.pm' (or similar) not 'Makefile.PL-CRITIC.pm'
# NEED FIX: properly handle existing file suffixes so 'Makefile.PL' becomes 'Makefile-PL-CRITIC.pm' (or similar) not 'Makefile.PL-CRITIC.pm'

                # save distribution Critic violations in phase 3 directory
                print "[$PID] ", 'SAVING: writing Critic violations file \'', $distribution_file_critic_path, '\'', "\n";
                open(my filehandle $DISTRIBUTION_FILE_CRITIC_FILE, '>', $distribution_file_critic_path) or do {
                    print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening distribution file Critic violations file \'' . $distribution_file_critic_path . '\' for writing; ' . $OS_ERROR . '; dying';
                    if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                    next DISTRIBUTION;
                };
                print $DISTRIBUTION_FILE_CRITIC_FILE '$distribution_file_critic_violations = ', Dumper($distribution_file_critic_violations), '1;', "\n";
                close $DISTRIBUTION_FILE_CRITIC_FILE or do {
                    print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing distribution file Critic violations file \'' . $distribution_file_critic_path . '\' after writing; ' . $OS_ERROR . '; dying';
                    if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                    $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                    next DISTRIBUTION;
                };
            }

            # assemble criticized Perl path; include severity number in file name, to allow for multiple curations at different severities
            my string $distribution_file_criticized_name = $distribution_file->{name} . '-CRITICIZED' . $self->{critic_severity} . '.pm';
            my string $distribution_file_criticized_path = catfile(getcwd(), $distribution_file->{prefix}, $distribution_file_criticized_name);
            print "[$PID] ", 'have $distribution_file_criticized_path = \'', $distribution_file_criticized_path, '\'', "\n";

            # save criticized Perl files in phase 3 directory, open
            print "[$PID] ", 'SAVING: writing criticized Perl file \'', $distribution_file_criticized_path, '\'', "\n";
            open(my filehandle $DISTRIBUTION_FILE_CRITICIZED_FILE, '>', $distribution_file_criticized_path) or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem opening distribution file criticized Perl file \'' . $distribution_file_criticized_path . '\' for writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };

            # split Perl file on newline character,
            # each line is stored in it's own string in an array for easily jumping to a specific line number
            my $distribution_file_lines_criticized = [ split(/\n/, $distribution_file_data) ];
            print "[$PID] ", 'have $distribution_file_lines_criticized = ', Dumper($distribution_file_lines_criticized), "\n";

            # loop through all violations, jump to violating lines and append
            foreach my Perl::Critic::Violation $violation (@{$distribution_file_critic_violations}) {
                my integer $line_number = $violation->{_location}->[0];
                print "[$PID] ", 'have $line_number = ', $line_number, "\n";

                # append violation policy and description directly to violating line
                $distribution_file_lines_criticized->[($line_number - 1)] .= 
                    '  # this line violates ' .
                    $violation->{_policy} .
                    ', with severity ' .
                    $violation->{_severity} .
                    ' out of 5' .
                    ', and description \'' .
                    $violation->{_description} .
                    '\'';

                # if present, append violation explanation directly to violating line;
                # array refs are actually Perl Best Practices page numbers, not helpful here
                if ((exists $violation->{_explanation}) and
                    (defined $violation->{_explanation}) and
                    not((ref $violation->{_explanation}) eq 'ARRAY')) {
                    $distribution_file_lines_criticized->[($line_number - 1)] .= 
                        ', and explanation \'' .
                        $violation->{_explanation} .
                        '\'';
                }

                print "[$PID] ", 'have criticized $distribution_file_lines_criticized->[($line_number - 1)] = ', "\n", $distribution_file_lines_criticized->[($line_number - 1)], "\n";
            }

            # save name & path & data of criticized file in memory, thereby marking this file as having been criticized
            $distribution_file->{criticized_name} = $distribution_file_criticized_name; 
            $distribution_file->{criticized_path} = $distribution_file_criticized_path; 
            $distribution_file->{criticized_data} = join("\n", @{$distribution_file_lines_criticized}) . "\n";

            # save criticized Perl files in phase 3 directory, write;
            # join all criticized (and uncriticized) lines, to undo the split() above 
            print $DISTRIBUTION_FILE_CRITICIZED_FILE $distribution_file->{criticized_data};

            # save criticized Perl files in phase 3 directory, close
            close $DISTRIBUTION_FILE_CRITICIZED_FILE or do {
                print "[$PID] ", $decision = 'ERROR EPGPTTRMCxxx: problem closing distribution file criticized Perl file \'' . $distribution_file_criticized_path . '\' after writing; ' . $OS_ERROR . '; dying';
                if ($self->{parallel}) { $fork_manager->finish(1, { errored => $decision }); }  # if parallelized, exit child process
                $distributions_rejected->{$distribution_name} = { errored => $decision };  # errored distributions are rejected
                next DISTRIBUTION;
            };

# THEN HERE: run test suite again to ensure no errors are introduced by criticizing, filter on failures?
# THEN HERE: run test suite again to ensure no errors are introduced by criticizing, filter on failures?
# THEN HERE: run test suite again to ensure no errors are introduced by criticizing, filter on failures?

        }  # end of phase 3 DISTRIBUTION_FILE_TO_CRITICIZE loop

        # [[[ PHASE 4: PPI ]]]
        # [[[ PHASE 4: PPI ]]]
        # [[[ PHASE 4: PPI ]]]

        # mkdir & chdir for phase 4
        $self->phase_mkdir_chdir(4);

        # use PPI to parse all the Perl files in this distribution
        DISTRIBUTION_FILE_TO_PARSE:
        foreach my Archive::Tar::File $distribution_file (@{$distribution_tarball_files}) {
            print "[$PID] ", 'top of phase 4 DISTRIBUTION_FILE_TO_PARSE loop', "\n";
#            print "[$PID] ", 'have $distribution_file = ', Dumper($distribution_file), "\n";

            my string $distribution_file_name = $distribution_file->{name};
            my string $distribution_file_path = $distribution_file->{path};
            print "[$PID] ", 'have $distribution_file_name = \'', $distribution_file_name, '\'', "\n";
            print "[$PID] ", 'have $distribution_file_path = \'', $distribution_file_path, '\'', "\n";

            # do not parse the tarball file itself; should be the first Archive::Tar::File object, should not have a 'data' property
            if ((not exists $distribution_file->{data}) or
                (not defined $distribution_file->{data})) {
                print "[$PID] ", 'no distribution file data, presumably a directory, no PPI for this directory', "\n";
                next DISTRIBUTION_FILE_TO_PARSE;
            }
            elsif ($distribution_file->{data} eq q{}) {
                print "[$PID] ", 'empty distribution file data, presumably an empty file, no PPI for this empty file', "\n";
                next DISTRIBUTION_FILE_TO_PARSE;
            }

            # do not parse non-Perl files 
            if (not $distribution_file->{is_perl}) {
                print "[$PID] ", 'presumably not Perl source code, no PPI for this file', "\n";
                next DISTRIBUTION_FILE_TO_PARSE;
            }
            else {
                print "[$PID] ", 'presumably Perl source code, yes PPI for this file', "\n";
            }

            # declare variable to hold to-be-parsed Perl source code; defined below as either criticized or uncriticized source code
            my string $distribution_file_data;

# VERY FIRST START HERE: add $distribution_file->{tidied_data} to logic below
# VERY FIRST START HERE: add $distribution_file->{tidied_data} to logic below
# VERY FIRST START HERE: add $distribution_file->{tidied_data} to logic below

            # re-create entire distribution by combining modified & unmodified files;
            # if modified version of this file exists, copy it
            if ((exists $distribution_file->{criticized_data}) and
                (defined $distribution_file->{criticized_data})) {
                print "[$PID] ", 'have criticized Perl source code', "\n";

# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
                $distribution_file_data = $distribution_file->{criticized_data};
            }
            # otherwise, copy original unmodified version
            else {
                print "[$PID] ", 'have uncriticized Perl source code', "\n";

# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
# FIRST START HERE: actually copy files into phase 4 directory; need to first create distribution directory inside phase 4 directory???
                $distribution_file_data = $distribution_file->{data};
            }

            # call PPI to parse Perl source code
            my PPI::Document $distribution_file_PPI_document = PPI::Document->new(\$distribution_file_data);
            my PPI::Dumper $distribution_file_PPI_dumper = PPI::Dumper->new($distribution_file_PPI_document);
            my string $distribution_file_PPI_string = $distribution_file_PPI_dumper->string();
            print "[$PID] ", 'have $distribution_file_PPI_string =', "\n", $distribution_file_PPI_string;

            # generate 4-tier training data, from coarsest to finest grained
                # DISTRIBUTION, LIST OF FILES
                # FILE, LIST OF PACKAGES etc
                # PACKAGE, LIST OF SUBROUTINES etc
                # SUBROUTINE, LIST OF LINES etc

            # format as markdown

            # save files to disk

        }  # end of phase 4 DISTRIBUTION_FILE_TO_PARSE loop





        # prepare this distribution as accepted;
        # only store file and directory paths in memory, all actual data is stored in the files, to avoid using all memory and freezing OS
        my hashref $distribution_accepted = {};
        $distribution_accepted->{metacpan_path} = $distribution_metacpan_path;
        $distribution_accepted->{tarball_path} = $distribution_tarball_path;
        $distribution_accepted->{tarball_files_path} = $distribution_tarball_files_path;
        $distribution_accepted->{kwalitee_path} = $distribution_kwalitee_path;
        $distribution_accepted->{critic_paths} = $distribution_critic_paths;
        print "[$PID] ", 'have $distribution_accepted = ', Dumper($distribution_accepted), "\n";

        # if parallelized, exit child process and return accepted distribution
        if ($self->{parallel}) {
            # exit in the child process(es), default exit code is 0;
            # return to-be-accepted distribution info to parent, will be saved as accepted by parent
            $fork_manager->finish(0, $distribution_accepted);
        }
        else {
            # distributions that encountered errors, warnings, or filters are rejected above; all others are accepted here
            $distributions_accepted->{$distribution_name} = $distribution_accepted;
            print 'ACCEPTING: all filters passed, distribution \'', $distribution_name, '\' will be accepted', "\n";
        }

#last DISTRIBUTION;  # TMP DEBUG, ONLY PROCESS ONE DISTRIBUTION
    }

    # if parallelized, wait for all child processes to exit before all iterations of the DISTRIBUTION loop are truly finished
    if ($self->{parallel}) {
        print "[$PID] ", 'PARALLEL: after bottom of DISTRIBUTION loop, about to call $fork_manager->wait_all_children()...', "\n";
        $fork_manager->wait_all_children();
    }

    print 'Done processing MetaCPAN API results!', "\n";

    # dump output data for both rejected and accepted distributions
    print 'have $distributions_rejected = ', Dumper($distributions_rejected), "\n";
    print 'have $distributions_accepted = ', Dumper($distributions_accepted), "\n";

    # prepare abbreviated at-a-glance output data
    my string::arrayref $distributions_rejected_names = [sort keys %{$distributions_rejected}];
    my integer $distributions_rejected_count = scalar @{$distributions_rejected_names};
    print 'have $distributions_rejected_count = ', $distributions_rejected_count, "\n";
    print 'have $distributions_rejected_names = ', Dumper($distributions_rejected_names), "\n";
    my string::arrayref $distributions_accepted_names = [sort keys %{$distributions_accepted}];
    my integer $distributions_accepted_count = scalar @{$distributions_accepted_names};
    print 'have $distributions_accepted_count = ', $distributions_accepted_count, "\n";
    print 'have $distributions_accepted_names = ', Dumper($distributions_accepted_names), "\n";

    # change current working directory back to base directory
    chdir $self->{base_directory} or
       die 'ERROR EPGPTTRMCxxx: problem changing back to base directory \'', $self->{base_directory}, '\'; ', $OS_ERROR ,'; dying';
    print 'changed back to base directory \'', $self->{base_directory}, '\'', "\n";

    # assemble paths for rejected and accepted distributions
    my string $distributions_rejected_path = catfile(getcwd(), 'REJECTED.pm');
    print 'have $distributions_rejected_path = \'', $distributions_rejected_path, '\'', "\n";
    my string $distributions_accepted_path = catfile(getcwd(), 'ACCEPTED.pm');
    print 'have $distributions_accepted_path = \'', $distributions_accepted_path, '\'', "\n";

    # save rejected distributions in base directory, overwriting (AKA clobbering) existing file if present
    print 'SAVING: writing rejected distributions file \'', $distributions_rejected_path, '\'', "\n";
    open(my filehandle $DISTRIBUTIONS_REJECTED_FILE, '>', $distributions_rejected_path) or
        die 'ERROR EPGPTTRMCxxx: problem opening rejected distributions file \'', $distributions_rejected_path, '\' for writing; ', $OS_ERROR ,'; dying';
    print $DISTRIBUTIONS_REJECTED_FILE '$distributions_rejected_count = ', $distributions_rejected_count, ';', "\n";
    print $DISTRIBUTIONS_REJECTED_FILE '$distributions_rejected_names = ', Dumper($distributions_rejected_names), "\n";
    print $DISTRIBUTIONS_REJECTED_FILE '$distributions_rejected = ', Dumper($distributions_rejected), '1;', "\n";
    close $DISTRIBUTIONS_REJECTED_FILE, or
        die 'ERROR EPGPTTRMCxxx: problem closing rejected distributions file \'', $distributions_rejected_path, '\' after writing; ', $OS_ERROR ,'; dying';

    # save accepted distributions in base directory, overwriting (AKA clobbering) existing file if present
    print 'SAVING: writing accepted distributions file \'', $distributions_accepted_path, '\'', "\n";
    open(my filehandle $DISTRIBUTIONS_ACCEPTED_FILE, '>', $distributions_accepted_path) or
        die 'ERROR EPGPTTRMCxxx: problem opening accepted distributions file \'', $distributions_accepted_path, '\' for writing; ', $OS_ERROR ,'; dying';
    print $DISTRIBUTIONS_ACCEPTED_FILE '$distributions_accepted_count = ', $distributions_accepted_count, ';', "\n";
    print $DISTRIBUTIONS_ACCEPTED_FILE '$distributions_accepted_names = ', Dumper($distributions_accepted_names), "\n";
    print $DISTRIBUTIONS_ACCEPTED_FILE '$distributions_accepted = ', Dumper($distributions_accepted), '1;', "\n";
    close $DISTRIBUTIONS_ACCEPTED_FILE, or
        die 'ERROR EPGPTTRMCxxx: problem closing accepted distributions file \'', $distributions_accepted_path, '\' after writing; ', $OS_ERROR ,'; dying';

    # save in memory and return accepted CPAN distributions; overwrite previously-curated distributions in memory, if any;
    # wait to overwrite previous distributions until now when all data has been processed, in case of errors
    $self->{distributions_rejected} = $distributions_rejected;
    $self->{distributions_accepted} = $distributions_accepted;

    # return only the accepted distributions
    return $self->{distributions_accepted};
}


# PGPTTRMC00x
sub phase_mkdir_chdir {
# mkdir & chdir for a specific phase of curation
    { my void $RETURN_TYPE };
    ( my PerlGPT::Curator::MetaCPAN $self, my integer $phase ) = @ARG;

    print "[$PID] ", 'in phase_mkdir_chdir(), received $phase = \'', $phase, '\'', "\n";

    # error or warning if no phase
    if (not defined $phase) {
        die "[$PID] ", 'ERROR EPGPTTRMC100a: undefined phase, dying';
    }
    elsif (($phase < 0) or
        ($phase > ((scalar @{$self->{phases}}) - 1))) {
        die "[$PID] ", 'ERROR EPGPTTRMC100b: invalid phase, dying';
    }

    my string $phase_directory = catdir($self->{base_directory}, $self->{phases}->[$phase]);
    print "[$PID] ", 'in phase_mkdir_chdir(), have $phase_directory = \'', $phase_directory, '\'', "\n";

    # create phase directory with error checking
    $self->mkdir_error_check($phase_directory);

    # change current working directory to phase directory
    chdir $phase_directory or
       die "[$PID] ", 'ERROR EPGPTTRMC101: problem changing to Phase ', $phase, ' directory \'', $phase_directory, '\'; ', $OS_ERROR ,'; dying';
    print "[$PID] ", 'in phase_mkdir_chdir(), changed to Phase ', $phase, ' directory \'', $phase_directory, '\'', "\n";

    # no return value
    return;
}


# PGPTTRMC00x
sub mkdir_error_check {
# mkdir with error checking
    { my void $RETURN_TYPE };
    ( my PerlGPT::Curator::MetaCPAN $self, my string $directory_path ) = @ARG;

    print "[$PID] ", 'in mkdir_error_check(), received $directory_path = \'', $directory_path, '\'', "\n";

    # error or warning if no directory_path
    if (not defined $directory_path) {
        die "[$PID] ", 'ERROR EPGPTTRMC200a: undefined directory_path, dying';
    }
    elsif ($directory_path eq q{}) {
        die "[$PID] ", 'ERROR EPGPTTRMC200b: invalid directory_path, dying';
    }

    # check if directory path exists, create if not
    if (-d $directory_path) {
        # directory path already exists
        print "[$PID] ", 'in mkdir_error_check(), directory \'', $directory_path, '\' already exists', "\n";
    }
    elsif (-e $directory_path) {
        # error, non-directory
        die "[$PID] ", 'ERROR EPGPTTRMC201: non-directory \'', $directory_path, '\' already exists where directory should be, dying';
    }
    else {
        # create directory path;
        # DEV NOTE: mkdir does not create parent directories, make_path() does
#        mkdir $directory_path or
        make_path($directory_path) or
            die "[$PID] ", 'ERROR EPGPTTRMC202: problem creating directory \'', $directory_path, '\'; ', $OS_ERROR ,'; dying';
        print "[$PID] ", 'in mkdir_error_check(), created directory \'', $directory_path, '\'', "\n";
    }

    # no return value
    return;
}

1;
