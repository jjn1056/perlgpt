# COPYRIGHT
# ABSTRACT: PerlGPT LLM
# [[[ HEADER ]]]
package PerlGPT;
use strict;
use warnings;
our $VERSION = 0.001_000;

# [[[ INCLUDES ]]]
use Perl::Types;  # use Data::Dumper;  use English;

# [[[ OO INHERITANCE ]]]
use parent qw(Perl::Class);
use Perl::Class;

# [[[ OO PROPERTIES ]]]
our hashref $properties = {};

1;
