# perlgpt
PerlGPT

Large language model for Perl software development

## Developers Only
```
# install static dependencies
$ dzil authordeps | cpanm
$ dzil listdeps | cpanm

# document changes & insert copyrights before CPAN release
$ vi Changes       # include latest release info, used by [NextRelease] and [CheckChangesHasContent] plugins
$ vi dist.ini      # update version number
$ vi FOO.pm foo.t  # add "# COPYRIGHT" as first  line of file, used by [InsertCopyright] plugin
$ vi foo.pl        # add "# COPYRIGHT" as second line of file, used by [InsertCopyright] plugin

# build & install dynamic dependencies & test before CPAN release
$ dzil build
$ ls -ld PerlGPT*
$ cpanm --installdeps ./PerlGPT-FOO.tar.gz  # install dynamic dependencies if any exist
$ dzil test  # may need dependencies installed by above `cpanm` commands

# inspect build files before CPAN release
$ cd PerlGPT-FOO
$ ls -l
$ less Changes
$ less LICENSE
$ less COPYRIGHT
$ less CONTRIBUTING
$ less MANIFEST
$ less README.md
$ less README
$ less META.json
$ less META.yml

# make CPAN release
$ git add -A; git commit -av  # CPAN Release, vX.YYY; Codename FOO, BAR Edition
$ git push origin main
$ dzil release  # will build, test, prompt for CPAN upload, and create/tag/upload new git commit w/ only version number as commit message
```

## Original Creation
PerlGPT was originally created via the following commands:

```
# normal installation procedure for minting profile
$ cpanm Dist::Zilla::MintingProfile

# normal minting procedure
$ dzil new PerlGPT
```

## License & Copyright
PerlGPT is Free & Open Source Software (FOSS), please see the LICENSE and COPYRIGHT and CONTRIBUTING files for legal information.
